# Belajar DevOps #

[![Intro Devops](img/01-intro-devops.jpg)](img/01-intro-devops.jpg)


## Perintah Git ##

1. Membuat repository local

    ```
    git init
    ```

2. Melihat kondisi repo local

    ```
    git status
    ```

3. Menambahkan file ke staging area

    ```
    git add namafile
    ```

4. Menyimpan isi staging area ke local repo

    ```
    git commit -m "keterangan perubahan"
    ```

5. Mendaftarkan remote repository

    ```
    git remote add <nama-alias-repo> <url-repo>
    ```

    Contoh :

    ```
    git remote add gitlab git@gitlab.com:training-devops-2023-01/materi-devops.git
    ```

6. Mengupload isi branch `main` di local repo ke branch `main` di remote repo

    ```
    git push gitlab main
    ```
